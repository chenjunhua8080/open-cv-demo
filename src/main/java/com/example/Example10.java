package com.example;

import org.bytedeco.opencv.global.opencv_core;
import org.bytedeco.opencv.global.opencv_highgui;
import org.bytedeco.opencv.global.opencv_imgproc;
import org.bytedeco.opencv.opencv_core.Mat;
import org.bytedeco.opencv.opencv_core.Rect;
import org.bytedeco.opencv.opencv_core.Scalar;
import org.bytedeco.opencv.opencv_core.Size;

/**
 * Example10-画矩形，与、或、非、异或
 */
public class Example10 {

    public static void main(String[] args) {
        //创建图片1
        Mat mat1 = Mat.zeros(new Size(256, 256), opencv_core.CV_8UC3).asMat();
        //创建矩形1
        opencv_imgproc.rectangle(
            mat1,
            new Rect(50, 50, 100, 100),
            Scalar.RED,
            opencv_imgproc.FILLED,
            opencv_imgproc.LINE_8,
            0
        );
        //展示图片-mat1
        opencv_highgui.imshow("Example10-mat1", mat1);

        //创建图片2
        Mat mat2 = Mat.zeros(new Size(256, 256), opencv_core.CV_8UC3).asMat();
        //创建矩形2
        opencv_imgproc.rectangle(
            mat2,
            new Rect(256 - 150, 256 - 150, 100, 100),
            Scalar.YELLOW,
            opencv_imgproc.FILLED,
            opencv_imgproc.LINE_8,
            0
        );
        //展示图片-mat2
        opencv_highgui.imshow("Example10-mat2", mat2);

        //创建图片3
        Mat mat3 = Mat.zeros(new Size(256, 256), opencv_core.CV_8UC3).asMat();

        int index = 0;
        //开始/暂停
        boolean run = true;
        //循环键盘事件
        while (true) {
            //等待1000ms，接收按键响应
            int key = opencv_highgui.waitKey(1000);
            //输出按键值
            System.out.println(index);
            //Esc
            if (key == 27) {
                return;
            } else if (key == 32) {
                run = !run;
            } else {
                if (run) {
                    //与、或、非、异或
                    switch (index++) {
                        case 0:
                            opencv_core.bitwise_and(mat1, mat2, mat3);
                            break;
                        case 1:
                            opencv_core.bitwise_or(mat1, mat2, mat3);
                            break;
                        case 2:
                            opencv_core.bitwise_xor(mat1, mat2, mat3);
                            break;
                        case 3:
                            opencv_core.bitwise_not(mat1, mat3);
                            break;
                        case 4:
                            opencv_core.bitwise_not(mat2, mat3);
                            break;
                        default:
                    }
                    if (index >= 5) {
                        index = 0;
                    }
                    //展示图片-mat3
                    opencv_highgui.imshow("Example10-mat3", mat3);
                }
            }
        }
    }
}
