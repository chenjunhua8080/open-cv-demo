package com.example;

import java.util.Arrays;
import org.bytedeco.opencv.global.opencv_core;
import org.bytedeco.opencv.global.opencv_highgui;
import org.bytedeco.opencv.opencv_core.Mat;
import org.bytedeco.opencv.opencv_core.Scalar;
import org.bytedeco.opencv.opencv_core.Size;

/**
 * Example4-图片像素的读写操作
 */
public class Example4 {

    public static void main(String[] args) {
        //创建空白图片获得Mat，32*32 8bit 无符号整型 3通道
        Mat org = new Mat(new Size(32, 32), opencv_core.CV_8UC3, new Scalar(0, 255, 255, 1));
        //副本
        Mat mat = org.clone();
        System.out.println("宽度：" + mat.cols());
        System.out.println("高度：" + mat.rows());
        System.out.println("通道：" + mat.channels());
        //展示图片像素
        byte[] channels = new byte[mat.channels()];
        for (int j = 0; j < mat.rows(); j++) {
            for (int i = 0; i < mat.cols(); i++) {
//                System.out.print(mat.ptr(j, i).get() + ", ");

                //根据通道数量获取
                byte[] values = new byte[mat.channels()];
                mat.ptr(i, j).get(values);
                System.out.print(Arrays.toString(values) + " -> ");

                //修改赋值
                for (int k = 0; k < values.length; k++) {
                    //取反
                    values[k] = (byte) (255 - values[k]);
                }
                mat.ptr(i, j).put(values);
                System.out.print(Arrays.toString(values) + "\t\t");
            }
            System.out.println();
        }

        //展示图片-org
        opencv_highgui.namedWindow("Example4-org", opencv_highgui.WINDOW_AUTOSIZE);
        opencv_highgui.imshow("Example4-org", org);
        //展示图片-mat
        opencv_highgui.namedWindow("Example4-mat", opencv_highgui.WINDOW_AUTOSIZE);
        opencv_highgui.imshow("Example4-mat", mat);

        //一直等待，直到按下任意键
        opencv_highgui.waitKey(0);
        //销毁弹窗
        opencv_highgui.destroyAllWindows();
    }
}
