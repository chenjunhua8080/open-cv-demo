package com.example;

import org.bytedeco.opencv.global.opencv_core;
import org.bytedeco.opencv.global.opencv_highgui;
import org.bytedeco.opencv.global.opencv_imgcodecs;
import org.bytedeco.opencv.global.opencv_imgproc;
import org.bytedeco.opencv.opencv_core.Mat;
import org.bytedeco.opencv.opencv_core.Scalar;
import org.bytedeco.opencv.opencv_core.Size;

/**
 * Example12-色彩空间转换2-抠图
 */
public class Example12 {

    public static void main(String[] args) {
        //读取图片
        String file = "C:\\Users\\Allen\\Pictures\\JavaCV\\logo.png";
        Mat org = opencv_imgcodecs.imread(file);

        //创建图片
//        Mat mat = org.clone();
        Mat mat = Mat.zeros(new Size(256, 256), opencv_core.CV_8UC3).asMat();
        //hsv
        opencv_imgproc.cvtColor(org, mat, opencv_imgproc.COLOR_BGR2HSV);
        opencv_imgproc.resize(mat, mat, new Size(256, 256));
        opencv_highgui.imshow("Example12-mat", mat);

        //inRange：将在两个阈值内的像素值设置为白色（255），而不在阈值区间内的像素值设置为黑色（0）
        Mat mask = new Mat();
        opencv_core.inRange(
            mat,
//            # (下限: 绿色33/43/46,红色156/43/46,蓝色100/43/46)
//            # (上限: 绿色77/255/255,红色180/255/255,蓝
//            色124/255/255)
            new Mat(new Scalar(156, 43, 46, 1)),
            new Mat(new Scalar(180, 255, 255, 1)),
            mask
        );
        opencv_highgui.imshow("Example12-mask", mask);

        //取反
        Mat mask2 = new Mat();
        opencv_core.bitwise_not(mask, mask2);
        opencv_highgui.imshow("Example12-mask2", mask2);

        //背景色
        Mat mat2 = Mat.zeros(new Size(256, 256), opencv_core.CV_8UC3).asMat();
        opencv_imgproc.resize(org, mat2, new Size(256, 256));

        Mat yellow = new Mat(mat2.size(), mat2.type(), Scalar.YELLOW);
//        mat2.copyTo(yellow, mask2);
        mat2.copyTo(yellow, mask);
        opencv_highgui.imshow("Example12-yellow", yellow);

        opencv_highgui.waitKey(0);

//        int index = 0;
//        //开始/暂停
//        boolean run = true;
//        //循环键盘事件
//        while (true) {
//            //等待1000ms，接收按键响应
//            int key = opencv_highgui.waitKey(1000);
//            //输出按键值
//            System.out.println(index);
//            //Esc
//            if (key == 27) {
//                return;
//            } else if (key == 32) {
//                run = !run;
//            } else {
//                if (run) {
//                    //与、或、非、异或
//                    switch (index++) {
//                        case 0:
//                            break;
//                        default:
//                    }
//                    if (index >= 5) {
//                        index = 0;
//                    }
//                    //展示图片-mat3
//                    opencv_highgui.imshow("Example12-mat3", mat3);
//                }
//            }
//        }
    }
}
