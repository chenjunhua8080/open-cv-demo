package com.example;

import org.bytedeco.javacpp.IntPointer;
import org.bytedeco.javacpp.Pointer;
import org.bytedeco.opencv.global.opencv_core;
import org.bytedeco.opencv.global.opencv_highgui;
import org.bytedeco.opencv.global.opencv_imgcodecs;
import org.bytedeco.opencv.opencv_core.Mat;
import org.bytedeco.opencv.opencv_core.Scalar;
import org.bytedeco.opencv.opencv_highgui.TrackbarCallback;

/**
 * Example7-对比度/图片融合
 *
 * 对比度是每个单位像素都乘以对应的值，导致像素之间的差距越来越大；亮的越亮，暗的越暗
 * 亮度是每个单位都加上对应的值，像素之间的差距不会变大；一起亮一起暗
 */
public class Example7 {

    public static void main(String[] args) {
        //读取图片
        String file = "C:\\Users\\Allen\\Pictures\\JavaCV\\logo.png";
        Mat mat = opencv_imgcodecs.imread(file);

        //加减乘除运算
//        mat = opencv_core.add(mat, new Scalar(50,50,50,1)).asMat();
//        mat = opencv_core.subtract(mat, new Scalar(50,50,50,1)).asMat();
//        mat = opencv_core.multiply(mat, 1.2).asMat();
//        mat = opencv_core.divide(mat, 1.2).asMat();
        //对比度/图片融合
//        mat = opencv_core.addWeighted().asMat();

        //创建窗口
        opencv_highgui.namedWindow("Example7-mat", opencv_highgui.WINDOW_AUTOSIZE);
        //创建trackbar1
        opencv_highgui.createTrackbar(
            "Trackbar1",
            "Example7-mat",
            new IntPointer(),//初始定位，但是设置了无效，而且数字很大
            100,
            new TrackbarCallback() {
                @Override
                public void call(int pos, Pointer userdata) {
                    System.out.println("Trackbar1---" + pos);
                    //展示图片-mat
                    opencv_highgui.imshow("Example7-mat", opencv_core.add(mat, Scalar.all(pos)).asMat());
                }
            },
            null
        );
        //创建trackbar2
        opencv_highgui.createTrackbar(
            "Trackbar2",
            "Example7-mat",
            new IntPointer(),//初始定位，但是设置了无效，而且数字很大
            200,
            new TrackbarCallback() {
                @Override
                public void call(int pos, Pointer userdata) {
                    System.out.println("Trackbar2---" + (pos / 100D));
                    Mat dst = Mat.zeros(mat.size(), mat.type()).asMat();
                    opencv_core.addWeighted(
                        mat,
                        pos / 100D,
                        Mat.zeros(mat.size(), mat.type()).asMat(),
                        0,
                        0,
                        dst
                    );
                    //展示图片-mat
                    opencv_highgui.imshow("Example7-mat", dst);
                }
            },
            null
        );
        //设置Trackbar最小值
        opencv_highgui.setTrackbarMin("Trackbar2", "Example7-mat", 100);
        //展示图片-mat
        opencv_highgui.imshow("Example7-mat", mat);

        opencv_highgui.waitKey();
    }
}
