package com.example;

import org.bytedeco.opencv.global.opencv_core;
import org.bytedeco.opencv.global.opencv_highgui;
import org.bytedeco.opencv.global.opencv_imgcodecs;
import org.bytedeco.opencv.global.opencv_imgproc;
import org.bytedeco.opencv.opencv_core.Mat;
import org.bytedeco.opencv.opencv_core.MatVector;
import org.bytedeco.opencv.opencv_core.Size;

/**
 * Example11-通道分离与合并
 */
public class Example11 {

    public static void main(String[] args) {
        //读取图片
        String file = "C:\\Users\\Allen\\Pictures\\JavaCV\\logo.png";
        Mat org = opencv_imgcodecs.imread(file);
        Mat mat = org.clone();
        opencv_imgproc.resize(org, mat, new Size(256, 256));
        //展示图片-org
        opencv_highgui.imshow("Example11-org", mat);

        //通道拆分
        MatVector matVector = new MatVector();
        opencv_core.split(mat, matVector);
        //展示图片-org
//        opencv_highgui.imshow("Example11-1", matVector.get(0));
//        opencv_highgui.imshow("Example11-2", matVector.get(1));
//        opencv_highgui.imshow("Example11-3", matVector.get(2));

        //通道合并
        Mat dst = mat.clone();
        MatVector matVector2 = new MatVector(3);
        //需要设置其他单通道颜色为0
        matVector2.put(0, Mat.zeros(mat.size().height(), mat.size().width(), opencv_core.CV_8UC1).asMat());
        matVector2.put(1, Mat.zeros(mat.size().height(), mat.size().width(), opencv_core.CV_8UC1).asMat());
        matVector2.put(2, matVector.get(2));
//        matVector2.put(matVector.get(1), matVector.get(1), matVector.get(0));
        System.out.println(matVector2.size());
        opencv_core.merge(matVector2, dst);
        opencv_highgui.imshow("Example11-4", dst);

        //通道混合
        Mat dst2 = Mat.zeros(mat.size(), mat.type()).asMat();
        opencv_core.mixChannels(
            mat,
            1,
            dst2,
            1,
            new int[]{0, 1, 1, 2, 2, 0},//交换
            3
        );
        opencv_highgui.imshow("Example11-5", dst2);

        opencv_highgui.waitKey(0);
    }
}
