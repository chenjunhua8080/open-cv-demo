package com.example;

import org.bytedeco.javacpp.IntPointer;
import org.bytedeco.javacpp.Pointer;
import org.bytedeco.opencv.global.opencv_core;
import org.bytedeco.opencv.global.opencv_highgui;
import org.bytedeco.opencv.global.opencv_imgcodecs;
import org.bytedeco.opencv.opencv_core.Mat;
import org.bytedeco.opencv.opencv_core.Scalar;
import org.bytedeco.opencv.opencv_highgui.TrackbarCallback;

/**
 * Example6-trackbar
 */
public class Example6 {

    public static void main(String[] args) {
        //读取图片
        String file = "C:\\Users\\Allen\\Pictures\\JavaCV\\logo.png";
        Mat mat = opencv_imgcodecs.imread(file);

        //加减乘除运算
//        mat = opencv_core.add(mat, new Scalar(50,50,50,1)).asMat();
//        mat = opencv_core.subtract(mat, new Scalar(50,50,50,1)).asMat();
//        mat = opencv_core.multiply(mat, 1.2).asMat();
//        mat = opencv_core.divide(mat, 1.2).asMat();

        //创建窗口
        opencv_highgui.namedWindow("Example6-mat", opencv_highgui.WINDOW_AUTOSIZE);
        //创建trackbar
        opencv_highgui.createTrackbar(
            "trackbar",
            "Example6-mat",
            new IntPointer(),//初始定位，但是设置了无效，而且数字很大
            100,
            new TrackbarCallback() {
                @Override
                public void call(int pos, Pointer userdata) {
                    System.out.println(pos);
                    //展示图片-mat
                    opencv_highgui.imshow("Example6-mat", opencv_core.add(mat, Scalar.all(pos)).asMat());
                }
            },
            null
        );
        //展示图片-mat
        opencv_highgui.imshow("Example6-mat", mat);

        opencv_highgui.waitKey();
    }
}
