package com.example;

import org.bytedeco.opencv.global.opencv_highgui;
import org.bytedeco.opencv.global.opencv_imgcodecs;
import org.bytedeco.opencv.global.opencv_imgproc;
import org.bytedeco.opencv.opencv_core.Mat;

/**
 * Example8-键盘事件
 */
public class Example8 {

    public static void main(String[] args) {
        //读取图片
        String file = "C:\\Users\\Allen\\Pictures\\JavaCV\\logo.png";
        Mat mat = opencv_imgcodecs.imread(file);

        //创建窗口
        opencv_highgui.namedWindow("Example8-mat", opencv_highgui.WINDOW_AUTOSIZE);

        //展示图片-mat
        opencv_highgui.imshow("Example8-mat", mat);

        Mat dst = Mat.zeros(mat.size(), mat.type()).asMat();
        //循环键盘事件
        while (true) {
            //等待1000ms，接收按键响应
            int key = opencv_highgui.waitKey(1000);
            //输出按键值
            System.out.println(key);
            switch (key) {
                //Esc
                case 27:
                    return;
                //1
                case 49:
                    opencv_highgui.imshow("Example8-mat", mat);
                    break;
                //2
                case 50:
                    opencv_imgproc.cvtColor(mat, dst, opencv_imgproc.COLOR_BGR2GRAY);
                    opencv_highgui.imshow("Example8-mat", dst);
                    break;
                //3
                case 51:
                    opencv_imgproc.cvtColor(mat, dst, opencv_imgproc.COLOR_BGR2HSV);
                    opencv_highgui.imshow("Example8-mat", dst);
                    break;
                default:
            }
        }
    }
}
