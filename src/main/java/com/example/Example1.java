package com.example;

import org.bytedeco.opencv.global.opencv_highgui;
import org.bytedeco.opencv.global.opencv_imgcodecs;
import org.bytedeco.opencv.opencv_core.Mat;

/**
 * Example1-图片的读取于展示
 */
public class Example1 {

    public static void main(String[] args) {
        String file = "C:\\Users\\Allen\\Pictures\\JavaCV\\logo.png";
        //读取图片
        Mat mat = opencv_imgcodecs.imread(file);
        //1.弹窗(可以解决图片太大无法展示全，但不能解决乱码问题)
        opencv_highgui.namedWindow("Example1", opencv_highgui.WINDOW_AUTOSIZE);
        //2.展示图片(没有1时也可以弹窗，有1是名字需要和1的名字相同)
        opencv_highgui.imshow("Example1", mat);
        //一直等待，直到按下任意键
        opencv_highgui.waitKey(0);
        //销毁弹窗
        opencv_highgui.destroyAllWindows();
    }

}
