package com.example;

import org.bytedeco.opencv.global.opencv_highgui;
import org.bytedeco.opencv.global.opencv_imgcodecs;
import org.bytedeco.opencv.global.opencv_imgproc;
import org.bytedeco.opencv.opencv_core.Mat;

/**
 * Example9-OpenCV自带颜色表
 */
public class Example9 {

    public static void main(String[] args) {
        //读取图片
        String file = "C:\\Users\\Allen\\Pictures\\JavaCV\\logo.png";
        Mat mat = opencv_imgcodecs.imread(file);

        //创建窗口
        opencv_highgui.namedWindow("Example9-mat", opencv_highgui.WINDOW_AUTOSIZE);

        //展示图片-mat
        opencv_highgui.imshow("Example9-mat", mat);

        Mat dst = Mat.zeros(mat.size(), mat.type()).asMat();
        //colorIndex -> opencv_imgproc.COLORMAP_AUTUMN
        int colorIndex = 0;
        //开始/暂停
        boolean run = true;
        //循环键盘事件
        while (true) {
            //等待1000ms，接收按键响应
            int key = opencv_highgui.waitKey(500);
            //输出按键值
            System.out.println(key);
            //Esc
            if (key == 27) {
                return;
            } else if (key == 32) {
                run = !run;
            } else {
                if (run) {
                    opencv_imgproc.applyColorMap(mat, dst, colorIndex++);
                    opencv_highgui.imshow("Example9-mat", dst);
                    if (colorIndex == 22) {
                        colorIndex = 0;
                    }
                }
            }
        }
    }
}
