package com.example;

import org.bytedeco.opencv.global.opencv_highgui;
import org.bytedeco.opencv.global.opencv_imgcodecs;
import org.bytedeco.opencv.global.opencv_imgproc;
import org.bytedeco.opencv.opencv_core.Mat;

/**
 * Example2-图片色彩转换
 */
public class Example2 {

    public static void main(String[] args) {
        //读取图片
        String file = "C:\\Users\\Allen\\Pictures\\JavaCV\\logo.png";
        Mat mat = opencv_imgcodecs.imread(file);

        //定义新图片
        Mat hsv = new Mat();
        Mat gray = new Mat();
        //颜色处理
        opencv_imgproc.cvtColor(mat, hsv, opencv_imgproc.COLOR_BGR2HSV);
        opencv_imgproc.cvtColor(mat, gray, opencv_imgproc.COLOR_BGR2GRAY);
        //保存图片
        opencv_imgcodecs.imwrite("C:\\Users\\Allen\\Pictures\\JavaCV\\logo-hsv.png", hsv);
        opencv_imgcodecs.imwrite("C:\\Users\\Allen\\Pictures\\JavaCV\\logo-gray.png", gray);

        //展示图片
        imgShow(mat, hsv, gray);

        //一直等待，直到按下任意键
        opencv_highgui.waitKey(0);
        //销毁弹窗
        opencv_highgui.destroyAllWindows();
    }

    private static void imgShow(Mat mat, Mat hsv, Mat gray) {
        //展示图片-原图
        opencv_highgui.namedWindow("Example2-org", opencv_highgui.WINDOW_AUTOSIZE);
        opencv_highgui.imshow("Example2-org", mat);
        //展示图片-hsv
        opencv_highgui.namedWindow("Example2-hsv", opencv_highgui.WINDOW_AUTOSIZE);
        opencv_highgui.imshow("Example2-hsv", hsv);
        //展示图片-gray
        opencv_highgui.namedWindow("Example2-gray", opencv_highgui.WINDOW_AUTOSIZE);
        opencv_highgui.imshow("Example2-gray", gray);
    }

}
