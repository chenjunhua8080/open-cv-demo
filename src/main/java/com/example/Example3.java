package com.example;

import org.bytedeco.opencv.global.opencv_core;
import org.bytedeco.opencv.global.opencv_highgui;
import org.bytedeco.opencv.global.opencv_imgcodecs;
import org.bytedeco.opencv.opencv_core.Mat;
import org.bytedeco.opencv.opencv_core.Scalar;
import org.bytedeco.opencv.opencv_core.Size;

/**
 * Example3-图片的创建与复制
 */
public class Example3 {

    public static void main(String[] args) {
        //1.通过读取图片获得Mat
        String file = "C:\\Users\\Allen\\Pictures\\JavaCV\\logo.png";
        Mat mat = opencv_imgcodecs.imread(file);

        //2.通过克隆获得Mat
        Mat mat1 = mat.clone();
        Mat mat2 = new Mat();
        mat.copyTo(mat2);

        //展示图片
//        imgShow(mat, mat1, mat2);

        /**
         * Mat的本质是一个矩阵数组
         *
         * opencv_core.CV_8UC3 啥意思？
         * CV_<bit_depth>(S|U|F)C<number_of_channels>
         *
         * 1、bit_depth：比特数，有代表8bite\16bite\32bite\64bite
         * 8表示你所创建的储存图片的Mat对象中，每个像素点在内存空间所占的空间大小8bite。
         *
         * 2、S|U|F
         * S: signed int，即有符号整型。
         * U: unsigned int，即无符号整型。
         * F: float，单精度浮点型。
         *
         * 3、<number_of_channels>：代表所存储的图片的通道数。
         * 若为1：grayImg灰度图像，即单通道图像。
         * 若为2：RGB彩色图像，即3通道图像。
         * 若为3：带Alpha通道的RGB彩色图像，即4通道图像。
         *
         * Scalar
         * 蓝、绿、红和透明度
         */
        //3.创建空白图片获得Mat，32*32 8bit 无符号整型 3通道
        Mat mat3 = new Mat(new Size(256, 256), opencv_core.CV_8UC3, new Scalar(0));
        System.out.println("宽度：" + mat3.cols());
        System.out.println("高度：" + mat3.rows());
        System.out.println("通道：" + mat3.channels());
        //展示图片-mat3
        opencv_highgui.namedWindow("Example3-mat3", opencv_highgui.WINDOW_AUTOSIZE);
        opencv_highgui.imshow("Example3-mat3", mat3);

        //一直等待，直到按下任意键
        opencv_highgui.waitKey(0);
        //销毁弹窗
        opencv_highgui.destroyAllWindows();
    }

    private static void imgShow(Mat mat, Mat mat1, Mat mat2) {
        //展示图片-原图
        opencv_highgui.namedWindow("Example3-org", opencv_highgui.WINDOW_AUTOSIZE);
        opencv_highgui.imshow("Example3-org", mat);
        //展示图片-m1
        opencv_highgui.namedWindow("Example3-mat1", opencv_highgui.WINDOW_AUTOSIZE);
        opencv_highgui.imshow("Example3-mat1", mat1);
        //展示图片-m2
        opencv_highgui.namedWindow("Example3-mat2", opencv_highgui.WINDOW_AUTOSIZE);
        opencv_highgui.imshow("Example3-mat2", mat2);
    }

}
