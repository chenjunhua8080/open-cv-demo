package com.example;

import org.bytedeco.opencv.global.opencv_core;
import org.bytedeco.opencv.global.opencv_highgui;
import org.bytedeco.opencv.global.opencv_imgcodecs;
import org.bytedeco.opencv.opencv_core.Mat;

/**
 * Example5-图片像素的算术操作
 */
public class Example5 {

    public static void main(String[] args) {
        //读取图片
        String file = "C:\\Users\\Allen\\Pictures\\JavaCV\\logo.png";
        Mat org = opencv_imgcodecs.imread(file);

        //副本
        Mat mat = org.clone();
        //加减乘除运算
//        mat = opencv_core.add(mat, new Scalar(50,50,50,1)).asMat();
//        mat = opencv_core.subtract(mat, new Scalar(50,50,50,1)).asMat();
//        mat = opencv_core.multiply(mat, 1.2).asMat();
        mat = opencv_core.divide(mat, 1.2).asMat();

        //展示图片-org
        opencv_highgui.namedWindow("Example5-org", opencv_highgui.WINDOW_AUTOSIZE);
        opencv_highgui.imshow("Example5-org", org);
        //展示图片-mat
        opencv_highgui.namedWindow("Example5-mat", opencv_highgui.WINDOW_AUTOSIZE);
        opencv_highgui.imshow("Example5-mat", mat);

        //一直等待，直到按下任意键
        opencv_highgui.waitKey(0);
        //销毁弹窗
        opencv_highgui.destroyAllWindows();
    }
}
